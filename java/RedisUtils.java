/**
 * Copyright (C), 2015-2019, XXX有限公司
 * FileName: RedisUtils
 * Author:   阿宇
 * Date:     2019/1/3 0003 15:44
 * Description: 操作Redis
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */

import redis.clients.jedis.Jedis;

/**
 * 〈一句话功能简述〉<br> 
 * 〈操作Redis〉
 *
 * @author 阿宇
 * @create 2019/1/3 0003
 * @since 1.0.0
 */
public class RedisUtils {

    private static Jedis jedis ;
    static{
        jedis = new Jedis("hadoop1",6379);
    }

    //设置数据
    public static void setData(String key,String value){
        jedis.mset(key,value);
    }
    //获取数据
    public static String getData(String key){
        return jedis.get(key);
    }
    //关闭连接
    public static void close(){
        jedis.close();
    }

}