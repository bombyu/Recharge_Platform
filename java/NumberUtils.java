/**
 * Copyright (C), 2015-2019, XXX有限公司
 * FileName: NumberUtils
 * Author:   阿宇
 * Date:     2019/1/3 0003 20:06
 * Description: 数字工具类
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */

/**
 * 〈一句话功能简述〉<br> 
 * 〈数字工具类〉
 *
 * @author 阿宇
 * @create 2019/1/3 0003
 * @since 1.0.0
 */
import java.math.BigDecimal;

/**
 * 数字格式化工具类
 *
 * @author Administrator
 */
public class NumberUtils {

    /**
     * 格式化小数
     *
     * @param num 浮点
     * @param scale 四舍五入的位数
     * @return 格式化小数
     */
    public static double formatDouble(double num, int scale) {
        /*
        Exception in thread "main" java.lang.NumberFormatException: Infinite or NaN
	at java.math.BigDecimal.<init>(BigDecimal.java:895)
	at java.math.BigDecimal.<init>(BigDecimal.java:872)
	at sessionanalyze.util.NumberUtils.formatDouble(NumberUtils.java:21)
	at spark.analyze.UserVisitSessionAnalyzeSpark$.calcuateAndPersistAggrStat(UserVisitSessionAnalyzeSpark.scala:382)
	at spark.analyze.UserVisitSessionAnalyzeSpark$.main(UserVisitSessionAnalyzeSpark.scala:111)
	at spark.analyze.UserVisitSessionAnalyzeSpark.main(UserVisitSessionAnalyzeSpark.scala)
         */
        if(Double.isNaN(num)){
            num = 0.0;
        }
        BigDecimal bd = new BigDecimal(num);
        return bd.setScale(scale, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

}