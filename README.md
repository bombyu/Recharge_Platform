# 充值平台实时统计分析

#### 介绍
1、统计全网的充值订单量, 充值金额, 充值成功数
2、全国各省充值业务失败量分布，统计每小时各个省份的充值失败数据量
3、以省份为维度统计订单量排名前 10 的省份数据,并且统计每个省份的订单成功率，只保留一位小数，存入MySQL中
4、实时统计每小时的充值笔数和充值金额
5、业务失败省份 TOP3（离线处理[每天]）
6、1)以省份为维度,统计每分钟各省的充值笔数和充值金额（离线处理）
   2)以省份为维度,统计每小时各省的充值笔数和充值金额（离线处理）


#### 软件架构
软件架构说明：FLume + Kafka + SparkStreaming + SparkCore + Redis + Mysql + ScalikeJDBC


#### 使用说明

1. Spark版本为2.2.X；Scala的版本需要2.11.X以上
2. 个人感觉ScalikeJDBC使用起来特别方便，嘻嘻
3. 代码可能存在一些小Bug，欢迎大佬指点迷津

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)